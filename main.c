#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <dirent.h>

void get_all_pids();

void inject_so(long malloc, long free, long dlopen);

void get_all_pids()
{
	DIR *procdir;
	FILE *fp;
	struct dirent *entry;
	char path[256 + 5 + 5];
	int pid;

	procdir = opendir("/proc");


	return;
}

int main(int argc, char **argv)
{
	
	return 0;
}

void ptrace_malloc(long malloc)
{
	// rsi contains the address of malloc...
	// we want to force the process to call it
	asm(
	);
	

}

void inject_so(long malloc, long free, long dlopen)
{
	// save free and dlopen
	asm(
		"push %rsi \n"
		"push %rdx"
	   );
	// call malloc
	asm(
		"push %r9 \n"
		"mov %rdi, %r9 \n"
		"mov %rcx, %rdi \n"
		"callq *%r9 \n"
		"pop %r9 \n"
		"int $3"
	   );
	// calling dlopen
	asm(
		"pop %rdx \n"
		"push %r9 \n"
		"mov %rdx, %r9 \n"
		"mov %rax, %rdi \n"
		"movabs $i, %rsi \n"
		"callq *%r9 \n"
		"pop %r9 \n"
		"int $3"
	   );


			




	return;
}
