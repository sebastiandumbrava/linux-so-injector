#include <stdio.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <sys/ptrace.h>
#include <sys/types.h>
#include <sys/user.h> 
#include <string.h>
#include "ptrace.h"



void ptrace_attach(pid_t pid)
{
	int waitpid_status;

	if (ptrace(PTRACE_ATTACH, pid, NULL, NULL) < 0) {
		perror("ptrace");
		exit(1);
	}

	if (waitpid(pid, &waitpid_status, WUNTRACED != pid)) {
		perror("waitpid");
		exit(1);
	}
			
	return;
}

void ptrace_detach(pid_t pid)
{
	if (ptrace(PTRACE_DETACH, pid, NULL, NULL) < 0) {
		perror("ptrace");
		exit(1);
	}

	return;
}

void ptrace_getregs(pid_t pid, struct user_regs_struct *regs)
{
	if (ptrace(PTRACE_GETREGS, pid, NULL, regs) < 0) {
		perror("ptrace");
		exit(1);
	}

	return;
}

void ptrace_continue(pid_t pid)
{
	if (ptrace(PTRACE_CONT, pid, NULL, NULL) < 0) {
		perror("ptrace");
		exit(1);
	}

	return;
}

void ptrace_setregs(pid_t pid, struct user_regs_struct *regs)
{
	if (ptrace(PTRACE_SETREGS, pid, NULL, regs) < 0) {
		perror("ptrace");
		exit(1);
	}

	return;
}

void ptrace_read(pid_t pid, void *addr, void *buf, int len)
{
	int offset = 0;
	long word = 0;

	while (offset < len) {
		word = ptrace(PTRACE_PEEKTEXT, pid, addr + offset, NULL);
		if (word < 0) {
			perror("ptrace");
			exit(1);
		}
		memcpy(buf + offset, &word, sizeof(word));
		offset += sizeof(word);
	}

	return;
}

void ptrace_write(pid_t pid, void *addr, void *buf, int len)
{
	int offset = 0;
	long word = 0;

	while (offset < len) {
		memcpy(&word, buf + offset, sizeof(word));
		word = ptrace(PTRACE_POKETEXT, pid, addr + offset, word);
		if (word < 0) {
			perror("ptrace");
			exit(1);
		}
		offset += sizeof(word);
	}

	return;
}


