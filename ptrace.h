
void ptrace_attach(pid_t pid);
void ptrace_detach(pid_t pid);
void ptrace_get_regs(pid_t pid, struct user_regs_struct regs); 
